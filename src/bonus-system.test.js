import calculateBonuses from "./bonus-system.js";
const assert = require("assert");


describe('Bonuses tests', () => {
    let app;
    console.log("Tests started");

    test('Standard Low',  (done) => {
        let program = "Standard";
        let amount = 5000;
        assert.equal(calculateBonuses(program,amount), 0.05 * 1);
        done();
    });

    test('Premium Low',  (done) => {
        let program = "Premium";
        let amount = 5000;
        assert.equal(calculateBonuses(program,amount), 0.1 * 1);
        done();
    });

    test('Diamond Low',  (done) => {
        let program = "Diamond";
        let amount = 5000;
        assert.equal(calculateBonuses(program,amount), 0.2 * 1);
        done();
    });

    test('Invalid Low',  (done) => {
        let program = "Invalid";
        let amount = 5000;
        assert.equal(calculateBonuses(program,amount), 0);
        done();
    });

    test('Standard Medium',  (done) => {
        let program = "Standard";
        let amount = 10000;
        assert.equal(calculateBonuses(program,amount), 0.05 * 1.5);
        done();
    });

    test('Premium Medium',  (done) => {
        let program = "Premium";
        let amount = 10000;
        assert.equal(calculateBonuses(program,amount), 0.1 * 1.5);
        done();
    });

    test('Diamond Medium',  (done) => {
        let program = "Diamond";
        let amount = 10000;
        assert.equal(calculateBonuses(program,amount), 0.2 * 1.5);
        done();
    });

    test('Invalid Medium',  (done) => {
        let program = "Invalid";
        let amount = 10000;
        assert.equal(calculateBonuses(program,amount), 0);
        done();
    });

    test('Standard High',  (done) => {
        let program = "Standard";
        let amount = 50000;
        assert.equal(calculateBonuses(program,amount), 0.05 * 2);
        done();
    });

    test('Premium High',  (done) => {
        let program = "Premium";
        let amount = 50000;
        assert.equal(calculateBonuses(program,amount), 0.1 * 2);
        done();
    });

    test('Diamond High',  (done) => {
        let program = "Diamond";
        let amount = 50000;
        assert.equal(calculateBonuses(program,amount), 0.2 * 2);
        done();
    });

    test('Invalid High',  (done) => {
        let program = "Invalid";
        let amount = 50000;
        assert.equal(calculateBonuses(program,amount), 0);
        done();
    });

    test('Standard Extra High',  (done) => {
        let program = "Standard";
        let amount = 100000;
        assert.equal(calculateBonuses(program,amount), 0.05 * 2.5);
        done();
    });

    test('Premium Extra High',  (done) => {
        let program = "Premium";
        let amount = 100000;
        assert.equal(calculateBonuses(program,amount), 0.1 * 2.5);
        done();
    });

    test('Diamond Extra High',  (done) => {
        let program = "Diamond";
        let amount = 100000;
        assert.equal(calculateBonuses(program,amount), 0.2 * 2.5);
        done();
    });

    test('Invalid Extra High',  (done) => {
        let program = "Invalid";
        let amount = 100000;
        assert.equal(calculateBonuses(program,amount), 0);
        done();
    });

    console.log('Tests Finished');

});
